module Refinery
  module Hotels
    module Admin
      class HotelsController < ::Refinery::AdminController

        crudify :'refinery/hotels/hotel',
                :title_attribute => 'name', :xhr_paging => true


        def create
          # if the position field exists, set this object as last object, given the conditions of this class.

          if (@hotel = Refinery::Hotels::Hotel.create(params[:hotel])).valid?

            # If Photo Album Gem has been included, it will be create an album that has the name related with Promotion
           if defined?(::Refinery::PhotoAlbums)
             @hotel_album = Refinery::PhotoAlbums::PhotoAlbum.new
             @hotel_album.title = "Album Hotel #{@hotel.name}"
             @hotel_album.description = "Photo Album for Hotel #{@hotel.name}"
             @hotel_album.save
           end

           flash.notice = t(
              'refinery.crudify.created',
              :what => "#{@hotel.name}"
            )
              self.index
              @dialog_successful = true
              render :index
          else
            unless request.xhr?
              render :action => 'new'
            else
              render :partial => '/refinery/admin/error_messages', :locals => {
                       :object => @hotel,
                       :include_object_name => true
                     }
            end
          end
        end

        def destroy
          @hotel = Refinery::Hotels::Hotel.find(params[:id])
          # object gets found by find_#{singular_name} function
          title = "Hotel #{@hotel.name}"
          if @hotel.destroy
            if defined?(Refinery::Page)
              logger.debug("hahahaha")
            end
            flash.notice = t('destroyed', :scope => 'refinery.crudify', :what => "#{title}")
          end

          redirect_to refinery.hotels_admin_hotels_url
        end

      end
    end
  end
end
