module Refinery
  module Hotels
    module Admin
      class PromotionsController < ::Refinery::AdminController

        crudify :'refinery/hotels/promotion',
                :xhr_paging => true

        def callback
          @hotel = Refinery::Hotels::Hotel.find(params[:hotel_id])
          render :layout => false
        end

        def create
          if (@promotion = Refinery::Hotels::Promotion.create(params[:promotion])).valid?

            # If Photo Album Gem has been included, it will be create an album that has the name related with Promotion
            if defined?(::Refinery::PhotoAlbums)
              @promo_album = Refinery::PhotoAlbums::PhotoAlbum.new
              @promo_album.title = "Album Promotion #{@promotion.title}"
              @promo_album.description = "Photo Album for Promotion #{@promotion.title}"
              @promo_album.save
            end

            flash.notice = t(
              'refinery.crudify.created',
              :what => "#{@promotion.title}"
            )
              self.index
              @dialog_successful = true
              redirect_to refinery.hotels_admin_hotels_url
          else
            unless request.xhr?
              render :action => 'new'
            else
              render :partial => '/refinery/admin/error_messages', :locals => {
                       :object => @promotion,
                       :include_object_name => true
                     }
            end
          end
        end

        def destroy
          @promotion = Refinery::Hotels::Promotion.find(params[:id])
          title = "Promo #{@promotion.title}"
          if @promotion.destroy
            if defined?(::Refinery::PhotoAlbums)
              @promo_album = Refinery::PhotoAlbums::PhotoAlbum.find_by_title("Album Promotion #{@promotion.title}").destroy
            end
            flash.notice = t('destroyed', :scope => 'refinery.crudify', :what => "#{title}")
          end

          redirect_to refinery.hotels_admin_hotels_url
        end

      end
    end
  end
end
