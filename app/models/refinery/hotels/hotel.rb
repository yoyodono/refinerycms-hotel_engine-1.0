module Refinery
  module Hotels
    class Hotel < Refinery::Core::BaseModel
      self.table_name = 'refinery_hotels'

      attr_accessible :name, :description, :deletable, :position

      acts_as_indexed :fields => [:name, :description, :deletable]

      validates :name, :presence => true, :uniqueness => true

      has_many :promotions, :dependent => :destroy, :class_name => 'Refinery::Hotels::Promotion'
    end
  end
end
