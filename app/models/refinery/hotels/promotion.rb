module Refinery
  module Hotels
    class Promotion < Refinery::Core::BaseModel

      attr_accessible :title, :description, :caption, :hotel_id, :position

      acts_as_indexed :fields => [:title]

      validates :title, :presence => true, :uniqueness => {:scope => :hotel_id}

      belongs_to :hotel, :class_name => 'Refinery::Hotels::Hotel'
    end
  end
end
