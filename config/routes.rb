Refinery::Core::Engine.routes.append do

  # Frontend routes
  namespace :hotels do
    resources :hotels, :path => '', :only => [:index, :show]
  end

  # Admin routes
  namespace :hotels, :path => '' do
    namespace :admin, :path => 'refinery' do
      resources :hotels, :except => :show do
        get '/promotions/callback/' => "promotions#callback"
        resources :promotions
        collection do
          post :update_positions
        end
      end
    end
  end

end
