class CreateHotelsPromotions < ActiveRecord::Migration

  def up
    create_table :refinery_hotels_promotions do |t|
      t.string :title
      t.text :description
      t.string :caption
      t.integer :hotel_id
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-hotels"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/hotels/promotions"})
    end

    drop_table :refinery_hotels_promotions

  end

end
