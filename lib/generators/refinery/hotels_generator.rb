module Refinery
  class HotelsGenerator < Rails::Generators::Base
  #source_root File.expand_path('../hotels/templates/', __FILE__)

    def rake_db
      rake("refinery_hotels:install:migrations")
    end

    # def generate_hotels_initializer
    #  template "app/assets/images/refinery/icons/promotion_icon.png", File.join(destination_root, "app", "assets", "images", "refinery","icons", "promotion_icon.png")
    #end

    def append_load_seed_data
      create_file 'db/seeds.rb' unless File.exists?(File.join(destination_root, 'db', 'seeds.rb'))
      append_file 'db/seeds.rb', :verbose => true do
        <<-EOH

# Added by Refinery CMS Hotels extension
Refinery::Hotels::Engine.load_seed
        EOH
      end
    end
  end
end
