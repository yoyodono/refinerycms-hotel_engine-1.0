module Refinery
  module Hotels
    class Engine < Rails::Engine
      include Refinery::Engine
      isolate_namespace Refinery::Hotels

      engine_name :refinery_hotels

      initializer "register refinerycms_promotions plugin" do
        Refinery::Plugin.register do |plugin|
          plugin.name = "promotions"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.hotels_admin_promotions_path }
          plugin.pathname = root
          plugin.activity = {
            :class_name => :'refinery/hotels/promotion'
          }
          plugin.menu_match = %r{refinery/hotels/promotions(/.*)?$}
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::Promotions)
      end
    end
  end
end
