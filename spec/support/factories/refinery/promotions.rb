
FactoryGirl.define do
  factory :promotion, :class => Refinery::Hotels::Promotion do
    sequence(:title) { |n| "refinery#{n}" }
  end
end

